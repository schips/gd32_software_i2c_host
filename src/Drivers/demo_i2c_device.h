///////////////////////////////////////////////////////////////////////////////
//
// \brief
//  sw_slave_i2c.h
//
// \version
// v0.0.1: 2022.05.01, Initial version.
///////////////////////////////////////////////////////////////////////////////

#ifndef __SW_MAIN_I2C2_H_
#define __SW_MAIN_I2C2_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "i2c_main_core.h"

/**
 * @brief  Initialization board i2c2 interface
 * @param  none
 * @return none
 */
void demo_i2c_device_init(void);

/**
 * @brief  Set I2C Device address, for `demo_i2c_device write/read`, 
           you can skip this function if you want to use 'demo_i2c_bus write/read' only.
 * @param[in]   DevAddr_7Bit : I2C Device address(7BIT)
 * @return none
 */
void set_demo_i2c_device_addr(unsigned char DevAddr_7Bit);

/**
 * @brief  Read register from I2C Device
 * @param[in]   RegAddr : register
 * @param[out]  readVal : read value (if OK).
 * @return I2C_SUCCESS when readVal is set, or I2C_TIMEOUT when I2c Device not ack, or I2C_INVARGS when argument is NULL.
 */
I2C_Error_t demo_i2c_device_read_byte(unsigned char RegAddr, unsigned char  *readVal);

/**
 * @brief  Write register to I2C Device
 * @param[in]   RegAddr : register
 * @param[in]   readVal : write value.
 * @return I2C_SUCCESS when data is set, or I2C_TIMEOUT when I2c Device not ack;
 */
I2C_Error_t demo_i2c_device_write_byte(unsigned char RegAddr, unsigned char data);




/**
 * @brief  Read register from I2C bus (Linux-Like)
 * @param[in]   DevAddr_7Bit : I2C Device address(7BIT)
 * @param[in]   RegAddr : register
 * @param[out]  readVal : read value (if OK).
 * @return I2C_SUCCESS when readVal is set, or I2C_TIMEOUT when I2c Device not ack, or I2C_INVARGS when argument is NULL.
 */
I2C_Error_t demo_i2c_bus_read_byte(unsigned char DevAddr_7Bit, unsigned char RegAddr, unsigned char  *readVal);

/**
 * @brief  Write register I2C bus (Linux-Like)
 * @param[in]   DevAddr_7Bit : I2C Device address(7BIT)
 * @param[in]   RegAddr : register
 * @param[in]   readVal : write value.
 * @return I2C_SUCCESS when data is set, or I2C_TIMEOUT when I2c Device not ack;
 */
I2C_Error_t demo_i2c_bus_write_byte(unsigned char DevAddr_7Bit, unsigned char RegAddr, unsigned char data);












/**
 * @brief  Write registers to I2C Device
 * @param[in]   RegAddr : register
 * @param[in]   pData   : write values.
 * @param[in]   length  : the length of pData.
 * @return I2C_SUCCESS when readVal is set, or I2C_TIMEOUT when I2c Device not ack, or I2C_INVARGS when argument is NULL.
 * @return I2C_SUCCESS when data is set, or I2C_TIMEOUT when I2c Device not ack;
 */
unsigned char demo_i2c_device_write_bytes(unsigned char RegAddr, unsigned char *pData, unsigned short length);

/**
 * @brief  Read registers from I2C Device
 * @param[in]   RegAddr : register
 * @param[in]   pData   : array for saveing read value.
 * @param[in]   length  : the length of pData.
 * @return I2C_SUCCESS when readVal is set, or I2C_TIMEOUT when I2c Device not ack, or I2C_INVARGS when argument is NULL.
 */
unsigned char demo_i2c_device_read_bytes(unsigned char RegAddr, unsigned char *pData, unsigned short length);




/**
 * @brief  Write registers to I2C bus (Linux-Like)
 * @param[in]   DevAddr_7Bit : I2C Device address(7BIT)
 * @param[in]   RegAddr : register
 * @param[in]   pData   : write values.
 * @param[in]   length  : the length of pData.
 * @return I2C_SUCCESS when readVal is set, or I2C_TIMEOUT when I2c Device not ack, or I2C_INVARGS when argument is NULL.
 * @return I2C_SUCCESS when data is set, or I2C_TIMEOUT when I2c Device not ack;
 */
unsigned char demo_i2c_bus_write_bytes(unsigned char DevAddr_7Bit, unsigned char DevAddr, unsigned char RegAddr, unsigned char *pData, unsigned short length);

/**
 * @brief  Read registers from I2C Device
 * @param[in]   DevAddr_7Bit : I2C Device address(7BIT)
 * @param[in]   RegAddr : register
 * @param[out]  pData   : array for saveing read value.
 * @param[in]   length  : the length of pData.
 * @return I2C_SUCCESS when readVal is set, or I2C_TIMEOUT when I2c Device not ack, or I2C_INVARGS when argument is NULL.
 */
unsigned char demo_i2c_bus_read_bytes(unsigned char DevAddr_7Bit, unsigned char RegAddr, unsigned char *pData, unsigned short length);
#ifdef __cplusplus
}
#endif


#endif /*__SW_MAIN_I2C2_H_*/
