/*!
    \file    main.c
    \brief   led spark with systick delay in polling mode

    \version 2014-12-26, V1.0.0, platform GD32F1x0(x=3,5)
    \version 2016-01-15, V2.0.0, platform GD32F1x0(x=3,5,7,9)
    \version 2016-04-30, V3.0.0, firmware update for GD32F1x0(x=3,5,7,9)
    \version 2017-06-19, V3.1.0, firmware update for GD32F1x0(x=3,5,7,9)
    \version 2019-11-20, V3.2.0, firmware update for GD32F1x0(x=3,5,7,9)
    \version 2020-09-21, V3.3.0, firmware update for GD32F1x0(x=3,5,7,9)
    \version 2022-08-15, V3.4.0, firmware update for GD32F1x0(x=3,5)
*/

/*
    Copyright (c) 2022, GigaDevice Semiconductor Inc.

    Redistribution and use in source and binary forms, with or without modification, 
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this 
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice, 
       this list of conditions and the following disclaimer in the documentation 
       and/or other materials provided with the distribution.
    3. Neither the name of the copyright holder nor the names of its contributors 
       may be used to endorse or promote products derived from this software without 
       specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
OF SUCH DAMAGE.
*/

#include "gd32f1x0.h"
#include "systick.h"

#include "demo_i2c_device.h"

int main(void)
{
    unsigned char dev_address_7bit;
    I2C_Error_t i2c_ret;
    unsigned char  tmp;

    //nvic_priority_group_set(NVIC_PRIGROUP_PRE1_SUB3); 

    /* configure systick */
    systick_config();

    demo_i2c_device_init();



    dev_address_7bit = 0x58;

    while(1)
    {
#if 0
        set_demo_i2c_device_addr(dev_address_7bit);

        i2c_ret = demo_i2c_device_write_byte(0x00, 0xaa);
        if(i2c_ret != I2C_SUCCESS)
        {
            //printf("I2C write error\n");
        }
        i2c_ret = demo_i2c_device_read_byte(0x00, &tmp);
        if(i2c_ret != I2C_SUCCESS)
        {
            //printf("I2C read error\n");
        }

#else
        i2c_ret = demo_i2c_bus_write_byte(dev_address_7bit, 0x00, 0xaa);
        if(i2c_ret != I2C_SUCCESS)
        {
            //printf("I2C write error\n");
        }
        i2c_ret = demo_i2c_bus_read_byte(dev_address_7bit, 0x00, &tmp);
        if(i2c_ret != I2C_SUCCESS)
        {
            //printf("I2C read error\n");
        }
#endif
        delay_1ms(500);
    }
}

#include <stdio.h>
/* retarget the C library printf function to the USART */
int fputc(int ch, FILE *f)
{
    return ch;
    //usart_data_transmit(EVAL_COM0, (uint8_t)ch);
    //while(RESET == usart_flag_get(EVAL_COM0, USART_FLAG_TBE));
    //return ch;
}
